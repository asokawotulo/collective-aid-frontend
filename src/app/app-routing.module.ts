import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { VolunteersComponent } from './volunteers/volunteers.component';
import { TermsconditionsComponent } from './termsconditions/termsconditions.component';


const routes: Routes = [
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'data', loadChildren: './data/data.module#DataModule' },
  { path: 'about', component: AboutComponent },
  { path: 'volunteers', component: VolunteersComponent },
  { path: 'home', component: HomeComponent },
  { path: 'termsconditions', component: TermsconditionsComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
