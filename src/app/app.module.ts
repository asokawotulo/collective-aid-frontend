import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core'
import { ParallaxModule, ParallaxConfig } from 'ngx-parallax';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { AboutComponent } from './about/about.component';
import { AgmCoreModule } from '@agm/core';
import { VolunteersComponent } from './volunteers/volunteers.component';
import { TermsconditionsComponent } from './termsconditions/termsconditions.component';


@NgModule({
  declarations: [
  AppComponent,
  HomeComponent,
  HeaderComponent,
  FooterComponent,
  AboutComponent,
  VolunteersComponent,
  TermsconditionsComponent,
  ],
  imports: [
  BrowserModule,
  BrowserAnimationsModule,
  AppRoutingModule,
  ParallaxModule,
  HttpClientModule,
  AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDIvk05q8LTBSt96wl9jTIx6BEpueB7BrM'
    })
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
