import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})

export class HeaderComponent implements OnInit {

  private loggedIn

  constructor(private auth: AuthService, public nav: NavbarService) { }

  ngOnInit() {
    this.loggedIn = this.auth.isLoggedIn()
  }

}