import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DataRoutingModule } from './data-routing.module';

import { ParallaxModule } from 'ngx-parallax';
import { FormsModule } from '@angular/forms';
import { DetailsComponent } from './details/details.component';
import { EventComponent } from './event/event.component';
import { ProviderComponent } from './provider/provider.component';

@NgModule({
  declarations: [
    DetailsComponent,
    EventComponent,
    ProviderComponent
  ],
  imports: [
    CommonModule,
    DataRoutingModule,
    ParallaxModule,
    FormsModule
  ]
})
export class DataModule { }
