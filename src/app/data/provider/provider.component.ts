import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { FooterService } from '../../services/footer.service';
import { ApiService } from 'src/app/services/api.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {

  public orglist = []

  constructor(private api: ApiService, private router: Router, private nav: NavbarService, private foo: FooterService) { }

  ngOnInit() {
    this.nav.show()
    this.foo.show()
    this.getProvider()
  }

  // Get provider from API
  getProvider() {
    this.api.get('/provider').subscribe(
      res => {
        this.orglist = Object.values(res)
        console.log(this.orglist)
      },
      error => console.log(error)
    )
  }

  // When item is clicked
  getClickedOrg(id) {
    this.router.navigate(['/data/provider', id])
  }

}
