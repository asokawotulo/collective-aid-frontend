import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsComponent } from './details/details.component';
import { EventComponent } from './event/event.component';
import { ProviderComponent } from './provider/provider.component';

const routes: Routes = [
  { path: 'event', component: EventComponent },
  { path: 'provider', component: ProviderComponent },
  { path: ':item/:id', component: DetailsComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DataRoutingModule { }
