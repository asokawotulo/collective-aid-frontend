import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { FooterService } from '../../services/footer.service';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class EventComponent implements OnInit {

  private eventlist = []
  public filteredlist = []
  public filterlist = []
  private checklist = {
    1: false,
    2: false,
    3: false,
    4: false,
    5: false,
    6: false
  }

  constructor(private api: ApiService, private router: Router, private route: ActivatedRoute, private nav: NavbarService, private foo: FooterService) { }

  ngOnInit() {
    this.nav.show()
    this.foo.show()
    this.getEvents()
    this.getServices()
  }

  checkFilter(checkbox) {
    this.checklist[checkbox.name] = checkbox.checked
  }

  // Filter the Event
  filterEvent() {
    this.filteredlist = []
    for (let key1 in this.eventlist) {
      for (let key2 in this.eventlist[key1]['services']) {
        let service_id = this.eventlist[key1]['services'][key2]['service_id']
        // show event with service that match service_id
        for (let key3 in this.checklist) {
          if (this.checklist[key3]) {
            if (service_id == key3) {
              if (this.filteredlist.find((event) => event === this.eventlist[key1]) === undefined)
                this.filteredlist.push(this.eventlist[key1])
            }
          }
        }
      }
    }
    console.log(this.filteredlist)
  }

  // Get Service from API
  getServices() {
    this.api.get('/service').subscribe(
      res => {
        this.filterlist = Object.values(res)
      }
    )
  }

  // Get the Event from API
  getEvents() {
    this.api.get('/event').subscribe(
      res => {
        this.eventlist = Object.values(res)
        this.filteredlist = this.eventlist
        console.log(this.filteredlist)
      },
      error => console.log(error)
    )
  }

  // When Item is Clicked
  getClickedEvent(id) {
    this.router.navigate(['/data/event', id])
  }

}
