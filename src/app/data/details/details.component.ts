import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from 'src/app/services/api.service';
import { NavbarService } from 'src/app/services/navbar.service';
import { FooterService } from 'src/app/services/footer.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {

  private path
  private id
  private sub
  private data
  public event : boolean  = false
  public provider : boolean = false

  constructor(private route: ActivatedRoute, private api: ApiService, private nav: NavbarService, private foo: FooterService) { }

  ngOnInit() {
    this.nav.show()
    this.foo.show()
    this.path = (this.route.snapshot.url[0].path);
    // Check if it's event or provider
    console.log(this.path)
    if (this.path == 'event') {
      this.event = true
      this.provider = false
    }
    if (this.path == 'provider') {
      this.provider = true
      this.event = false
    }
    this.sub = this.route.params.subscribe(params => {
      this.id = +params['id']
    })
    this.getData()
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

  // Get Data to show
  getData() {
    this.api.get('/' + this.path + '/' + this.id).subscribe(
      res => {
        this.data = res
        console.log(this.data)
      }
    )
  }

}
