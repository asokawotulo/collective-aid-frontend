import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private jwthelper = new JwtHelperService()

  constructor(private api: ApiService) { }

  login(data) {
    return this.api.post('/auth/login', data)
  }

  register(data) {
    return this.api.post('/auth/register', data)
  }

  // Post JWT Token to do logout function
  logout() {
    const rawToken = localStorage.getItem('rawToken')
    const httpOptions = {
      params: {
        'token': rawToken
      }
    };
    return this.api.postheaders('/auth/logout', httpOptions);
  }

  // Check if User is logged in
  isLoggedIn() {
    let expirationDate = localStorage.getItem('expirationDate')
    if (expirationDate != null || parseInt(expirationDate) > Date.now()) return true
    else return false
  }

  // Store JWT Token to local storage
  storeJWTToken(res) {
    const rawToken = res['access_token']
    const expirationDate = this.jwthelper.getTokenExpirationDate(rawToken).getTime();

    localStorage.setItem('rawToken', rawToken)
    localStorage.setItem('expirationDate', JSON.stringify(expirationDate))
  }

  // Get refresh token from API
  getRefreshToken(httpOptions) {
    this.api.postheaders('/auth/refresh', httpOptions).subscribe(
      res => {
        console.log('Token refreshed')
        this.storeJWTToken(res)
      },
      error => console.log(error)
    )
  }

  // Check if token almost expired
  checkIfAlmostExp() {
    let expirationDate = localStorage.getItem('expirationDate')
    if (parseInt(expirationDate) - Date.now() <= 600000) return true
    else return false
  }

  // Refresh the token with a new one
  refreshToken() {
    console.log('isLoggedIn', this.isLoggedIn())
    console.log('checkIfAlmostExp', this.checkIfAlmostExp())
    if (this.isLoggedIn()) {
      const rawToken = localStorage.getItem('rawToken')
      const httpOptions = {
        headers: {
          'Authorization': 'Bearer ' + rawToken,
          'Accept': 'application/json'
        }
      }
      this.getRefreshToken(httpOptions)
    }
    else console.log('not logged in')
  }

}
