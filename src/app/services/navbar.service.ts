import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  visible: boolean;

  constructor() { this.visible = true; }

  // Hide/Show Navbar

  hide() { this.visible = false; }

  show() { this.visible = true; }

}
