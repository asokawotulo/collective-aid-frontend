import { Injectable, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiURL: string = 'https://api.thecollectiveaid.com/api';
  // private apiURL: string = 'http://128.199.190.235/api';

  constructor(private http: HttpClient) {
  }

  // Get from API
  get(path) {
    return this.http.get(this.apiURL + path)
  }

  // Post from API
  post(path, data) {
    return this.http.post(this.apiURL + path,  data)
  }

  // Post with Options
  postheaders(path, options) {
    return this.http.post(this.apiURL + path, null, options)
  }

}
