import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FooterService {

  visible: boolean;

  constructor() { this.visible = true; }

  // Hide/Show Footer

  hide() { this.visible = false; }

  show() { this.visible = true; }
  
}
