import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { FooterService } from '../../services/footer.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router, private auth: AuthService, private nav: NavbarService, private foo: FooterService) { }

  ngOnInit() {
    this.nav.hide()
    this.foo.hide()
  }

  // Check if input is not empty
  checkIfNotEmpty(form) {
    if (form.value.login == '' || form.value.password == '') {
      document.getElementById('validation').style.display = 'block'
      document.getElementById('validation').innerHTML = 'username or password field must not be empty'
      return false
    }
    else {
      document.getElementById('validation').style.display = 'none'
      return true
    }
  }

  // backend validation of the form
  validator(res) {
    let errormsg = res['error']['message']
    if (errormsg != null) {
      document.getElementById('validation').style.display = 'block'
      document.getElementById('validation').innerHTML = errormsg
    }
    else {
      document.getElementById('validation').style.display = 'none'
    }
  }
  
  //Call login function from Auth Service
  login(form) { 
    console.log(form.value)
    if (this.checkIfNotEmpty(form)) {
      this.auth.login(form.value).subscribe(
        res => {
          console.log(res)
          this.auth.storeJWTToken(res)
          this.router.navigate(['/auth/profile'])
        },
        error => this.validator(error)
        )
    }

  }

}

