import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../../services/navbar.service';
import { FooterService } from '../../services/footer.service';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  public currentTab = 1
  private filetoupload: File = null
  private formdata = new FormData()
  private picisvalid = {
    'picture': false,
    'identification': false,
    'cv': true
  }

  constructor(public auth: AuthService, private nav: NavbarService, private foo: FooterService) {
  }

  ngOnInit() {
    this.nav.hide()
    this.foo.hide()
  }

  // Next Step
  goNext() : void {
    this.currentTab += 1
  }

  // Back Step
  goBack() : void {
    this.currentTab -= 1
  }

  // Check if input is not empty
  checkIfNotEmpty(value, id) {
    if (value == "") {
      document.getElementById('val ' + id).style.display = 'block'
      document.getElementById('val ' + id).innerHTML = id + ' must not be empty'
      return false
    }
    else {
      document.getElementById('val ' + id).style.display = 'none'
      return true
    }
  }

  // Validation for first step
  validator1(form) : boolean {
    return this.checkIfNotEmpty(form.value.name, 'name')
  }

  // Validation for second step
  validator2(form) : boolean {
    let isvalid = {
      'username': false,
      'email': false,
      'password': false,
      'password_confirmation': false,
      'dob' : false
    }
    let dob = form.value.dob
    let mm = parseInt(dob.substring(0,2))
    let dd = parseInt(dob.substring(3,5))
    let yyyy = parseInt(dob.substring(6,10))

    if (this.checkIfNotEmpty(form.value.username, 'username')) isvalid['username'] = true
    if (this.checkIfNotEmpty(form.value.email, 'email')) {
      if (/^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$/.test(form.value.email) == false) {
        document.getElementById('val email').style.display = 'block'
        document.getElementById('val email').innerHTML = 'email is not valid'
      }
      else {
        document.getElementById('val email').style.display = 'none'
        isvalid['email'] = true
      }
    }
    if (this.checkIfNotEmpty(form.value.password, 'password')) {
      if (form.value.password.length < 8) {
        document.getElementById('val password').style.display = 'block'
        document.getElementById('val password').innerHTML = 'password must be more than 8 characters'
      }
      else {
        document.getElementById('val password').style.display = 'none'
        isvalid['password'] = true
      }
    }
    if (form.value.password_confirmation != form.value.password) {
      document.getElementById('val password_confirmation').style.display = 'block'
      document.getElementById('val password_confirmation').innerHTML = 'password does not match'
    }
    else {
      document.getElementById('val password_confirmation').style.display = 'none'
      isvalid['password_confirmation'] = true
    }

    if (dob.length != 10) {
      document.getElementById('val dob').style.display = 'block'
      document.getElementById('val dob').innerHTML = 'date is not valid (use MM/DD/YYYY format)'
    }
    else if (isNaN(mm) || isNaN(dd) || isNaN(yyyy) || mm > 12 || dd > 31)  {
      document.getElementById('val dob').style.display = 'block'
      document.getElementById('val dob').innerHTML = 'date is not a valid'
    }
    else {
      document.getElementById('val dob').style.display = 'none'
      isvalid['dob'] = true
    }
    
    for (let key in isvalid) {
      if (isvalid[key] == false) return false
    }
    return true
  }

  // Validation for third step
  validator3(form) {
    if (!this.checkIfNotEmpty(form.value.picture, "picture") || !this.checkIfNotEmpty(form.value.identification, "identification")) return false
    if (this.picisvalid['picture'] == true && this.picisvalid['identification'] == true) return true
  }

  // Handle Upload file input
  handleFileInput(file, key) {
    if (file.item(0) != null) {
      this.filetoupload = file.item(0)
      if (this.filetoupload.size > 2000000) {
        document.getElementById('val ' + key).style.display = 'block'
        document.getElementById('val ' + key).innerHTML = key + ' must not be over 2MB'
      }
      else {
        this.picisvalid[key] = true
        document.getElementById('val ' + key).style.display = 'none'
        this.formdata.append(key, this.filetoupload, this.filetoupload.name)
      }
    }
    else {
      document.getElementById('val ' + key).style.display = 'block'
      document.getElementById('val ' + key).innerHTML = key + ' must not be empty'
    }
  }

  // Append data to Form Data
  appendToFormData(form) {
    for ( let key in form.value ) {
      if ( key == 'identification' || key == 'picture' || key == 'cv' ) continue
      else {
        this.formdata.append(key, form.value[key])
      }
    }
    if ( this.currentTab == 1 ) {
      if (this.validator1(form)) this.goNext()
    }
    else if ( this.currentTab == 2 ) {
      if (this.validator2(form)) this.goNext()
    }
    else if (this.currentTab == 3) {
      if (this.validator3(form)) this.goNext()
    }
    else if (this.currentTab == 4) {
      if (this.picisvalid['cv'] == true) this.goNext()
    }
  }

  // Validation from back end
  backvalidator(res) {
    let errormsg = res['error']
    if (errormsg != null) {
      this.currentTab -= 4
      alert(errormsg)
    }
  }

  // Submit the FormData to API
  register() {
    this.auth.register(this.formdata).subscribe(
      res => {
        console.log(res)
      },
      error => {
        console.log(error)
        this.backvalidator(error)
      }
    )
  }

}
