import { Component, OnInit } from '@angular/core';
import { NavbarService } from 'src/app/services/navbar.service';
import { FooterService } from 'src/app/services/footer.service';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public name = "Name"

  constructor(private router: Router, private auth: AuthService, private nav: NavbarService, private foo: FooterService) { }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) this.router.navigate(['/auth/login'])
    this.nav.show()
    this.foo.show()
  }

  getUserData() {
    
  }

  // Call logout function from Auth Service
  logout() {
    this.auth.logout().subscribe(
      res => {
        console.log(res)
        localStorage.clear();
        this.router.navigate(['/home'])
      },
      error => console.log(error)
    )
  }

}
