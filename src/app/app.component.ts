import { Component, HostBinding } from '@angular/core';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    // animation triggers go here
  ]
})
export class AppComponent {
  title = 'The Collective Aid';

  constructor(private auth: AuthService) {
    // this.auth.refreshToken()
  }
  
}
