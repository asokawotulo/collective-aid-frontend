import { Component, OnInit } from '@angular/core';
import { NavbarService } from '../services/navbar.service';
import { FooterService } from '../services/footer.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit {

  constructor(public nav: NavbarService, public foo: FooterService) {
  	window.scrollTo(0,0)
  }

  ngOnInit() {
    this.foo.show()
    this.nav.show()
  }

}
